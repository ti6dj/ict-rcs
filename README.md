# README #

These scripts are intended for people participating the data-parallel programming course at ICT.


### How to use ###

#### internal use (intern.sh) ####
The script requires the following paramters:

+ command:
    + mount: mount remote folder (on xy) on your local machine
    + ssh: establish a ssh connection to xy
    + all: mount remote folder on your local machine and establish a ssh connection to xy
    + unmount: unmount the mounted remote folder (requires superuser rights)

+ computer:
    + xy3 : machine with AMD GPU
    + xy5 : machine with NVIDIA GPU

+ studX: your group login (eg. stud1)

+ folder: folder to (un)mount (direct or relative path)

#### external use (extern.sh) ####
The script requires the following paramters:

+ command:
    + mount: mount remote folder (on xy) on your local machine
    + ssh: establish a ssh connection to xy
    + all: mount remote folder on your local machine and establish a ssh connection to xy
    + unmount: unmount the mounted remote folder (requires superuser rights)

+ computer:
    + xy3 : machine with AMD GPU
    + xy5 : machine with NVIDIA GPU

+ RZ-Login: your RZ login (eg. sabc1234)

+ studX: your group login (eg. stud1)

+ folder: folder to (un)mount (direct or relative path)